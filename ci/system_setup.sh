#!/bin/bash

apt update

# Project deps
apt install perl libemail-valid-perl libnet-ip-perl libdata-validate-domain-perl -y

# Test deps
apt install libtest-exception-perl libtest-exception-perl libdevel-cover-perl make libpod-coverage-perl -y

# test
prove t/*.t

# Test coverage
for TFILE in t/*.t; do perl -MDevel::Cover $TFILE; done

# Get coverage
cover +ignore_re ^t/.*t$
