package ValueObject::Sex;

use strict;
use warnings;
use Carp;
use ValueObject::Sex::Exception::Invalid;
use base qw/ ValueObject::Base /;

my @enum = (
    'm',
    'male',
    'f',
    'female',
);

sub validate {
    my $self    = shift;
    my $value   = shift || '';
    croak (ValueObject::Sex::Exception::Invalid->new($value))
        unless (grep { lc($_) eq lc($value) } @enum);
    return 1;
}

1;

=head1 NAME

ValueObject::Sex - Value object to represent male/female sex

=head1 SYNOPSIS

  use ValueObject::Sex;

  my $vo = ValueObject::Sex->new('male');

=head1 DESCRIPTION

ValueObject::Sex provides Value Object for validation of sex
values 'male', 'm', 'female', 'f' (Case insensitive)

This is only to determine biological sex.

=head1 METHODS

=over 4

=item new($value = '')

Creates a new object

Throws ValueObject::Sex::Exception::Invalid if $value is invalid

=item validate()

Called during instantiation to validate $value

=item value()

Returns the raw value as passed in at instantiation

=back

=head1 SEE ALSO

L<ValueObject::Base>

=cut
