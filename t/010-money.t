#!/usr/bin/env perl

use strict;
use warnings;
use Test::More tests => 22;
use Test::Exception;

use FindBin qw($Bin);
use lib "$Bin/../lib";

# TODO: Handle negative values??

# Setup

require_ok('ValueObject::Finance::Money::Comma');
require_ok('ValueObject::Finance::Money::Period');
require_ok('ValueObject::Finance::Money');


my $valid_comma_money = '23,45';
my $valid_period_money = '57.45';

my $direct_comma = ValueObject::Finance::Money::Comma->new($valid_comma_money);
my $direct_period = ValueObject::Finance::Money::Period->new($valid_period_money);
my $factory_comma = ValueObject::Finance::Money->new($valid_comma_money);
my $factory_period = ValueObject::Finance::Money->new($valid_period_money);


# Instantiation Test

isa_ok($direct_comma, 'ValueObject::Finance::Money::Comma');
isa_ok($direct_comma, 'ValueObject::Finance::Money::Base');

isa_ok($direct_period, 'ValueObject::Finance::Money::Period');
isa_ok($direct_period, 'ValueObject::Finance::Money::Base');

isa_ok($factory_comma, 'ValueObject::Finance::Money::Comma');
isa_ok($factory_comma, 'ValueObject::Finance::Money::Base');

isa_ok($factory_period, 'ValueObject::Finance::Money::Period');
isa_ok($factory_period, 'ValueObject::Finance::Money::Base');



## Test return values

ok($direct_comma->value() eq $valid_comma_money, "value function for object '$valid_comma_money'");
ok($direct_comma eq $valid_comma_money, "stringify for object '$valid_comma_money'");

ok($direct_period->value() eq $valid_period_money, "value function for object '$valid_period_money'");
ok($direct_period eq $valid_period_money, "stringify for object '$valid_period_money'");


## Test difference valid formats

my $valid_pound_amount = '23';
my $valid_pound_full_value = '23.00';
my $valid_pound_object = ValueObject::Finance::Money::Period->new($valid_pound_amount);
ok(	$valid_pound_object->full_value() eq $valid_pound_full_value,
	"Full value '$valid_pound_full_value' returned for value '$valid_pound_amount'"
);

my $valid_pound_single_pence_amount = '45.6';
my $valid_pound_single_pence_full_value = '45.60';
my $valid_pound_single_pence_object = ValueObject::Finance::Money::Period->new($valid_pound_single_pence_amount);
ok($valid_pound_single_pence_object->full_value() eq $valid_pound_single_pence_full_value,
	"Full value '$valid_pound_single_pence_full_value' returned for value '$valid_pound_single_pence_amount'"
);


my $valid_euro_amount = '23';
my $valid_euro_full_value = '23,00';
my $valid_euro_object = ValueObject::Finance::Money::Comma->new($valid_euro_amount);
ok(	$valid_euro_object->full_value() eq $valid_euro_full_value,
	"Full value '$valid_euro_full_value' returned for value '$valid_euro_amount'"
);

my $valid_euro_single_cent_amount = '45,6';
my $valid_euro_single_cent_full_value = '45,60';
my $valid_euro_single_cent_object = ValueObject::Finance::Money::Comma->new($valid_euro_single_cent_amount);
ok($valid_euro_single_cent_object->full_value() eq $valid_euro_single_cent_full_value,
	"Full value '$valid_euro_single_cent_full_value' returned for value '$valid_euro_single_cent_amount'"
);

## Test Exceptions

throws_ok { ValueObject::Finance::Money::Period->new('sdfa') }
    'ValueObject::Finance::Money::Exception::InvalidPeriod',
    'Invalid string exception for Period Finance::Money';

throws_ok { ValueObject::Finance::Money::Comma->new('sdfa') }
    'ValueObject::Finance::Money::Exception::InvalidComma',
    'Invalid string exception for Comma Finance::Money';

throws_ok { ValueObject::Finance::Money->new('sdfa') }
    'ValueObject::Finance::Money::Exception::Invalid',
    'Invalid string exception for Finance::Money';
